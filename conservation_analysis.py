import cobra

filestring=open('BIGGmodels.txt').read()
files=filestring.split('\n')
files.remove('')

unbalanced_dict={}
missing_mass_balances={}
for file_name in files:
    model_string=file_name+'.sbml' 
    
    
    mod_orig=cobra.io.read_sbml_model(model_string)
    mod_orig.optimize()    
    
    di=mod_orig.reactions.__dict__
    
    
    unbalanced_list=[]
    unbalanced=0
    for rxn in list(di['_dict']):
        

        
        metabolites=list(mod_orig.reactions.__getattr__(rxn).metabolites.items())
            
        if len(metabolites)!=1:
            massbalance=mod_orig.reactions.__getattr__(rxn).check_mass_balance()
            if len(massbalance)!=0:
                unbalanced_list.append(rxn)
                print('Model: ',file_name)
                print('Unbalanced reaction: ',rxn)
                print('Imbalances in: ',massbalance)
                
                
                
            charge_counter=0

            for j in range(len(metabolites)):

                charge_counter+= (metabolites[j][0].charge * float(metabolites[j][1]))

            if charge_counter!=0:
                unbalanced+=1
                
    
                
    missing_mass_balances.update({file_name:unbalanced_list})
    unbalanced_dict.update({file_name:unbalanced})
    
    

for biggmodel in unbalanced_dict:
    if unbalanced_dict[biggmodel]==0:
        model_string=biggmodel+'.sbml'
                
        mod_orig=cobra.io.read_sbml_model(model_string)
        
        nonzeros=0
        mets=di=mod_orig.metabolites.__dict__
        for metname in list(mets['_dict']):
            
            if mod_orig.metabolites.__getattr__(metname).charge!=0:
                nonzeros+=1
                
        print(nonzeros)


