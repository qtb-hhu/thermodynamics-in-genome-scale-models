# thermodynamics in genome-scale models

This repository holds code to apply thermodynamics to genome-scale models.

In order to reproduce the displayed results from "Are microbes thermodynamically
optimised self-reproducing machines", you can use the bash script to download
the used available genome~scale metabolic models from the BiGG Database.

For Linux & Mac OS, use the bash script (download_models.sh) by:

* chmod +x download_models.sh (if necessary)
* bash download_models.sh

After execution, the download of the models in sbml format should start.


For running Python script, the versions we used are:
* Python 3.6
* Cobrapy 0.15.3
* matplotlib 3.0.3
* NumPy 1.16.3
* pandas 0.24.2
* seaborn 0.9.0

Now, the python scripts should all be executable from your IDE or console.

* "EnergyOfFormationBiomass.py" calculates black-box model parameters from
published genome-scale models

* "conservation_analysis.py" checks for mass- and charge-balances of all
reactions in a model

* "anabolism_calculation.py" calculates properties of anabolism
separated from catabolism