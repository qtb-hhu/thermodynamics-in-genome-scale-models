#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some functionalities to calculate the energy of formation in BIGG genome-scale
models
"""

import numpy as np
import cobra as cb
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os 

__author__ = 'Tim Nies'
__credit__='Tim Nies'
__liscence__=''
__vesion__='0.1'
__maintainer__='Tim Nies'

#Energies of formation for various chemical compounds (see Battley 1993)
Gf_B = {
        'Ammonia_aq': -26.57,
        'CO2_g':-394.36,
        'Glycerol_aq':-497.48,
        'H+_aq': -39.96,
        'HPO4_2-_aq':-1089.26,
        'HSO4-_aq':-756.01,
        'HS-_aq':12.05,
        'OH-_aq':-157.29,
        'N2_g': 0,
        'O2_g':0,
        'O2_aq':16.32,
        'KOH_c':-379.11,
        'K2SO4_c':-1321.43,
        'P4O10_c':-2697.84,
        'Succinic_acid_aq': -746.63,
        'H2O_lq':-237.18
        }

def get_biomass_rxn(model):
    """Extracts the biomass reaction"""
    idx = [i for i in range(len(model.reactions)) if model.reactions[i].id.startswith("BIOMASS")]
    return idx

def get_biomass(model,carbon_normalize=True):
    """
    Get the biomass composition of all biomass functions in a genome scale model
    """
    biomass = {}
    idx = get_biomass_rxn(model)
    for i in idx:
        b = model.reactions[i].check_mass_balance()
        elemental_composition = {}
        for j in ['C','H','O','N','P','S','charge']:
            try:
                elemental_composition[j] = b[j]
            except:
                elemental_composition[j] = 0
        if carbon_normalize:
            bio = {k:v/elemental_composition["C"] for k,v in elemental_composition.items()}
        else:
            bio = elemental_composition
        biomass[model.reactions[i].id] = bio
    return biomass


def get_degree_of_reduction(cpd_elements,N_source='N2',y_C_ratio=False):
    """
    Calculates the degree of reduction of a compound.
    Input:  Element list of compound [C,H,O,N,P,S,charge]
    Output: Degree of reduction
    """
    if N_source == 'N2':
        y = 4*cpd_elements['C'] + cpd_elements['H'] - 2*cpd_elements['O'] + \
        0*cpd_elements['N'] + 5*cpd_elements['P'] + 6*cpd_elements['S'] - \
        cpd_elements['charge'] 
    elif N_source == 'NH4':
        y = 4*cpd_elements['C'] + cpd_elements['H'] - 2*cpd_elements['O'] - \
        3*cpd_elements['N'] + 5*cpd_elements['P'] + 6*cpd_elements['S'] - \
        cpd_elements['charge']
    else:
        raise NameError("%s is not a valid nitrogen source" % N_source)
    if y_C_ratio:
        y/cpd_elements[0]
    return y


def get_energy_of_combustion(y):
    """
    Calculates energy of combustion according to Battley1993. Other formulas
    can be found in Stockar1993 and Minkevich1973 
    """
    dG = y*-107.90
    return dG


def get_combustion_stoichiometries_of_biomass(biomass): #TODO: Find formula that describes combustion 
    """
    Assumes follwing combustion reaction /see Battley 1993
    biomass + a O2 + b KOH -> c CO2 + d N2 + e P4O10 + f K2SO4 + g H20
    """
    biomass = list(biomass.values())
    biomass = biomass[:-1]
    a = np.array([
        [0,0,1,0,0,0,0], #C
        [0,1,0,0,0,0,2], #H
        [2,1,2,0,10,4,1], #O
        [0,0,0,2,0,0,0], #N
        [0,0,0,0,4,0,0], #P
        [0,0,0,0,0,1,0], #S
        [0,1,0,0,0,2,0]]) #K
    b = np.append(biomass,0)
    x = np.linalg.solve(a,b)
    return x


def get_energy_of_formation_of_biomass(biomass,NH4=False):
    """Calculates the energy of formation for mass by using above defined functions
    Input: Biomass dictionary
    Output: Combustion stoichiometry, degree of reduction, Energy of combustion,
    Energy of formation
    """
    stoich = get_combustion_stoichiometries_of_biomass(biomass)
    if NH4==False:
        y = get_degree_of_reduction(biomass)
    else:
        y = get_degree_of_reduction(biomass,N_source='NH4')
    Gc = get_energy_of_combustion(y)
    Gf = Gc - stoich[0]*Gf_B['O2_g'] - stoich[1]*Gf_B['KOH_c'] - stoich[2]*Gf_B['CO2_g'] - \
    stoich[3]*Gf_B['N2_g'] - stoich[4]*Gf_B['P4O10_c'] - stoich[5]*Gf_B['K2SO4_c'] - \
    stoich[6]*Gf_B['H2O_lq']
    return (stoich,y,Gc,-Gf) #minus Gf because biomass is a substrate in the combustion reaction #-O2 because -*-=+ (see combustion stoichiometry)

###############################################################################
###############################################################################
    

model_ids = os.listdir('.')

analysis = {}

for i in model_ids:
    try:
        if 'sbml' in i:
            model = cb.io.read_sbml_model( './' + i) # CHANGE TO load_sbml WHEN NEEDED.
            biomass = get_biomass(model)
            print(i)
            for j in biomass:
                comp = biomass[j]
                comb_rxn,y,G_C,G_f = get_energy_of_formation_of_biomass(biomass[j])
                analysis[i+'_'+j] = {'Degree of reduction':y,'Energy of Combustion [KJ/C-mol]':G_C,
                        'Energy of formation [KJ/C-mol]': G_f}
    except:
        continue


analysis = pd.DataFrame(analysis).T
analysis_means = analysis.mean()


ax,fig = plt.subplots(figsize=(15,12))
sns.scatterplot(x='Degree of reduction',y='Energy of formation [KJ/C-mol]', data=analysis, color ='red', alpha=0.5,s=300)
plt.xlabel('Degree of reduction ($\gamma$)', fontsize=20)
plt.ylabel('Energy of formation $\Delta_f G_b$ [kJ/C-mol]',fontsize=20)
plt.xlim(3,8.5)
plt.ylim(-300,250)
plt.hlines(analysis_means[2],0,8.5,linestyle='dashed')
plt.vlines(analysis_means[0],-300,250,linestyles='dashed')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.annotate('Mean $\Delta G_f$',(3.2,-65), size=20)
plt.annotate('Mean $\gamma$',(5.05,205),rotation=90,size=20)
plt.savefig('Energy_of_formation_BIGGdatabase.png')
plt.show()