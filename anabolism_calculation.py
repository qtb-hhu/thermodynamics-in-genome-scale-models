

import cobra
import model_modification as MBmodel
import matplotlib.pyplot as plt
import re

#Some models produce infeasible solutions. Their identifiers are contained in this list.
troublemakers=[]
#The anabolic properties analysed are added into this dictionary.
anabolic_properties={}

#Create a list containing the model identifiers.
filestring=open('BIGGmodels.txt').read()
files=filestring.split('\n')
files.remove('')



for file_name in files:
    
    # Construction of object including the model properties for later modification
    print('Building Model')
    m=MBmodel.Struct_Model()
    m.SBMLtoModelbase(file_name+'.sbml')
    
    #reading model into cobrapy, optimization with default objectives
    original_model=cobra.io.read_sbml_model(file_name+'.sbml')
    model_name=original_model.name
    original_model.optimize()
    
    
    #To perform analysis of the relative carbon dioxide exchange, we select models that exhibit non-zero carbon dioxide exchange flux 
    if original_model.reactions.EX_co2_e.flux!=0.0:
        di=original_model.reactions.__dict__

        #collecting the identities of objective reaction(s)
        objective_list=[]
        for rxnname in list(di['_dict']):
            if original_model.reactions.__getattr__(rxnname).objective_coefficient!=0:
                obj_val=original_model.reactions.__getattr__(rxnname).flux
                objective_list.append(rxnname)
    
        
        #initiate a fake ATP compound to replace ATP in reactions that can produce ATP in model object
        m.set_cpdAttributes('atp_fake','fake atp',{'Unknown':1},None,'c','Unknown','Unknown',0.,[])
        
        print('Faking ATP production')
        #Change the stoichiometries of reaction that can theoretically produce ATP by replacing ATP with fake atp.
        for i in list(m.stoichiometries):
            if 'M_amp_c' in m.stoichiometries[i] and 'M_atp_c' in m.stoichiometries[i] and 'M_adp_c' in m.stoichiometries[i]:
                    m.stoichiometries[i]['atp_fake'] = m.stoichiometries[i].pop('M_atp_c')
                    
            if 'M_adp_c' in m.stoichiometries[i] and 'M_atp_c' in m.stoichiometries[i]:
                if abs(m.stoichiometries[i]['M_adp_c']) == abs(m.stoichiometries[i]['M_atp_c']) and m.stoichiometries[i]['M_atp_c']>m.stoichiometries[i]['M_adp_c']:# and m.reversible[i]=='irrev':
                    m.stoichiometries[i]['atp_fake'] = m.stoichiometries[i].pop('M_atp_c')
                            
                elif abs(m.stoichiometries[i]['M_adp_c']) == abs(m.stoichiometries[i]['M_atp_c']) and m.stoichiometries[i]['M_atp_c']<m.stoichiometries[i]['M_adp_c'] and m.reversible[i]=='rev':            
                    m.stoichiometries[i]['atp_fake'] = m.stoichiometries[i].pop('M_atp_c')
                                
            if 'M_amp_c' in m.stoichiometries[i] and 'M_atp_c' in m.stoichiometries[i]:
                if abs(m.stoichiometries[i]['M_amp_c']) == abs(m.stoichiometries[i]['M_atp_c']) and m.stoichiometries[i]['M_atp_c']>m.stoichiometries[i]['M_amp_c']:# and m.reversible[i]=='irrev':
                    m.stoichiometries[i]['atp_fake'] = m.stoichiometries[i].pop('M_atp_c')
        
                elif abs(m.stoichiometries[i]['M_amp_c']) == abs(m.stoichiometries[i]['M_atp_c']) and m.stoichiometries[i]['M_atp_c']<m.stoichiometries[i]['M_amp_c'] and m.reversible[i]=='rev':            
                    m.stoichiometries[i]['atp_fake'] = m.stoichiometries[i].pop('M_atp_c')
        
        #Construct a cobrapy object from the modified model
        print('Build Cobra model')
        mod=m.modelbase_to_cobra('id')
        mod.reactions.__getattr__('R_'+objective_list[-1]).bounds=(obj_val,obj_val)
        
        #Introducing exchange reactions for ATP+Water and ADP+Pi+Proton
        print('Adding free atp')
        reaction1=cobra.Reaction('EX_atp_c')
        reaction1.name = 'atp import '
        reaction1.lower_bound = -1000.  
        reaction1.upper_bound = 1000.  
        reaction1.add_metabolites({
            mod.metabolites.M_atp_c: -1.0,
            mod.metabolites.M_h2o_c: -1.0
        })
        
        
        reaction2=cobra.Reaction('EX_adp_c')
        reaction2.name = 'adp export '
        reaction2.lower_bound = 0. 
        reaction2.upper_bound = 1000.  
        reaction2.add_metabolites({
            mod.metabolites.M_adp_c: -1.0,
            mod.metabolites.M_pi_c: -1.0,
            mod.metabolites.M_h_c: -1.0
        })
        
    
        #Add a constraint that forces the flux of ATP and ADP exchange reactions to carry the same flux.
        #This was only the energy of ATP can be used, but no charge or mass.
        mod.add_reactions([reaction1,reaction2])
        same_flux = mod.problem.Constraint(
            -1*(mod.reactions.EX_atp_c.flux_expression) - mod.reactions.EX_adp_c.flux_expression,
            lb=0,
            ub=0)
        mod.add_cons_vars(same_flux)
        
        
        
        print('Min. Objective')
        list_of_exchange_reactions={}
        highest_carbon_influx={}
        rxn_degree_of_reduction={}

        carbons_counter=0
        # Counting the netto carbons in objective (usually biomass) with respect to stoichiometric coefficients and flux
        for rxnname in list(di['_dict']):
            if original_model.reactions.__getattr__(rxnname).objective_coefficient!=0:
                metabolitelist=list(original_model.reactions.__getattr__(rxnname).metabolites.items())
                
                for i in range(len(metabolitelist)):
                    bm_met=str(metabolitelist[i][0])
                    bm_stoich=float(metabolitelist[i][1])
                    form_list=re.split('(\d+)',original_model.metabolites.__getattr__(bm_met).formula)
                    if 'C' in form_list:
                        if form_list[form_list.index('C')+1].isdigit():

                            carbons_counter+=(float(form_list[form_list.index('C')+1])*original_model.reactions.__getattr__(rxnname).flux)*bm_stoich
        print('CARBONS IN BM: ',carbons_counter)
                            
                        
        #Finding exchange reactions that can potentially introduce carbohydrates into the system.
        #These reactions receive objective coefficients and are optimized
        #The negative stoichiometry of exchange reactions results in flux minimisation of exchange reactions.
        for rxnname in list(di['_dict']):
            influx_carbons=0
            if rxnname.startswith('EX_') and original_model.reactions.__getattr__(rxnname).lower_bound!=0:
                if len(original_model.reactions.__getattr__(rxnname).metabolites.items())==1:
                    metabolitelist=list(original_model.reactions.__getattr__(rxnname).metabolites.items())
                    exchanged_metabolite=str(metabolitelist[0][0])
                    

                    
                    if 'C' in original_model.metabolites.__getattr__(exchanged_metabolite).formula and 'H' in original_model.metabolites.__getattr__(exchanged_metabolite).formula:


                        mod.reactions.__getattr__('R_'+rxnname).upper_bound=0.
                        mod.reactions.__getattr__('R_'+rxnname).objective_coefficient=1 
        
        
        #optimization of the modified model. 
        mod.optimize()
        solution=mod.optimize()
        if solution.status=='infeasible':
            print('infeasible')
            troublemakers.append(file_name)
        elif carbons_counter==0:
            print('zero carbon objective')
            troublemakers.append(file_name)            
        else:
            
            try:
                #Results of feasible solutions are stored in the data dict and then passed into the anabolic_properties dict
                mod.summary()
                
                
                data={'ATP_needed':(mod.reactions.EX_adp_c.flux/abs(carbons_counter)),
                      'ATPflux':mod.reactions.EX_adp_c.flux,
                      'CO2_balance':mod.reactions.R_EX_co2_e.flux/original_model.reactions.EX_co2_e.flux,
                      'Name':model_name}
                

                anabolic_properties.update({file_name:data})
            
                print(data)

            
            except ValueError:
                troublemakers.append(file_name)


        


fig, ax = plt.subplots()

ax.axvline((0.316/2.33),color='k',linestyle='--')
ax.text((0.316/2.33)+0.01,5,'from Battley 1993',fontsize=15)

color = 'tab:blue'
ax.set_ylabel('ATP pro C-mol Biomass',fontsize=25)  # we already handled the x-label with ax1
for i in anabolic_properties:
    if anabolic_properties[i]['CO2_balance']>=0:

        ax.scatter(anabolic_properties[i]['CO2_balance'],anabolic_properties[i]['ATP_needed'], alpha=0.4,color=color,s=250)
ax.tick_params(axis='y',labelsize=15)
ax.tick_params(axis='x',labelsize=15)

ax.set_xlabel(r'$\frac{Anabolic\/\/CO_{2}\/\/released}{Metabolic\/\/CO_{2}\/\/released}$',fontsize=28)

plt.show()