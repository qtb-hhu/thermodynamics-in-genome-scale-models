__author__ = 'Nima'

import numpy as np

import itertools

import re

import pickle

from collections import defaultdict

import pprint

import pandas as pd

from libsbml import *
from libsbml import SBMLReader

import libsbml
import cobra
import math

class Struct_Model(object):

    @staticmethod
    def idx(list):
        return {it: id for id, it in enumerate(list)}


    def __init__(self):
        
        self.reactionID=[]
        self.reactionNames={}
        self.lower_bounds={}
        self.upper_bounds={}
        self.subsystems={}
        self.stoichiometries = {}
        self.reversible={}

        self.cpdID=[]
        self.cpdNames = {}
        self.cpdFormula = {}
        self.cpdCharge = {}
        self.cpdCompartment={}        
        self.reactionCommonName={}
        self.cpdCommonName={}
        self.cpdType={}
        self.cpdSMILE={}
        self.cpdGibbs={}
        self.cpdDBinfo={}
        self.reactionGibbs={}
        self.reactionDBinfo={}
        self.reactionEC={}
        self.reactionPathways={}
        self.reactionEnzymes={}



        
    def set_reaction(self,reactionID,  reactionName, LB=0,UB=1000,subsystem=None,stoichometry_dict={},reversibility='irrev',gibbs=0,ec=[],path=[],enzyme=[]):
        for i in list(stoichometry_dict.keys()):
            new_met_name=i
            if new_met_name[0].isdigit()==True:
                new_met_name='c_'+new_met_name
            
            new_met_name=new_met_name.replace(".","_")
            new_met_name=new_met_name.replace("-","__")
            new_met_name=new_met_name.replace("+","")
            
            if new_met_name!=i:
                stoichometry_dict[new_met_name] = stoichometry_dict[i]
                del stoichometry_dict[i]

        new_rxn_name=reactionID
        if new_rxn_name[0].isdigit()==True:
            new_rxn_name='v_'+new_rxn_name
        new_rxn_name=new_rxn_name.replace(".","_")
        new_rxn_name=new_rxn_name.replace("-","__")
        new_rxn_name=new_rxn_name.replace("+","")
        self.add_reaction(new_rxn_name)
        self.set_reactionName(new_rxn_name,reactionID)
        self.set_reactionCommonName(new_rxn_name,reactionName)
        self.set_lowerbound(new_rxn_name,LB)
        self.set_upperbound(new_rxn_name,UB)
        self.set_subsystem(new_rxn_name,subsystem)
        self.set_stoichiometry(new_rxn_name, stoichometry_dict)
        self.set_reversibility(new_rxn_name, reversibility)     
        self.reactionGibbs[new_rxn_name]=gibbs
        self.reactionEC[new_rxn_name]=ec
        self.reactionPathways[new_rxn_name]=path
        self.reactionEnzymes[new_rxn_name]=enzyme

    def add_reaction(self, reactionID):
            '''
            adds a single compound with name cpdName (string) to cpdNames
            if it does not yet exist
            '''
            self.reactionID.append(reactionID)
        
    def return_reaction(self,ID):
        return [ID, self.reactionNames[ID],self.lower_bounds[ID],self.upper_bounds[ID], self.subsystems[ID], self.stoichiometries[ID],self.reversible[ID],self.reactionGibbs[ID],self.reactionEC[ID],self.reactionPathways[ID],self.reactionEnzymes[ID]]





    
    def set_subsystem(self,rateID,Subsystem):
        self.subsystems[rateID] = Subsystem


    def set_upperbound(self, rateID, UB):
        
        self.upper_bounds[rateID] = UB

    def set_lowerbound(self, rateID, LB):
        
        self.lower_bounds[rateID] = LB
        

        

    def set_reactionName(self, rateID, rateName):
        
        self.reactionNames[rateID] = rateName

    def set_reactionCommonName(self, rateID, rateName):
        
        self.reactionCommonName[rateID] = rateName

        
    def set_cpdAttributes(self, cpdID, cpdName,formula,charge,compartment,typeC,SMILE,gibbs,db ):
        new_met_name=cpdID
        if new_met_name[0].isdigit()==True:
            new_met_name='c_'+new_met_name
        
        new_met_name=new_met_name.replace(".","_")
        new_met_name=new_met_name.replace("-","__")
        new_met_name=new_met_name.replace("+","")
        
        self.add_cpd(new_met_name)
        self.cpdNames[new_met_name] = cpdName 
        self.cpdCommonName[new_met_name]=cpdName
        
        self.cpdFormula[new_met_name] = formula
        self.cpdCharge[new_met_name] = charge
        self.cpdCompartment[new_met_name] = compartment
        self.cpdType[new_met_name] = typeC
        self.cpdSMILE[new_met_name] = SMILE
        self.cpdGibbs[new_met_name] = gibbs
        self.cpdDBinfo[new_met_name] = db
        
    def return_cpd(self,ID):
        return [ID, self.cpdNames[ID], self.cpdFormula[ID], self.cpdCharge[ID], self.cpdCompartment[ID],self.cpdType[ID],self.cpdSMILE[ID],self.cpdGibbs[ID],self.cpdDBinfo[ID]]
    

    def updateCpdIds(self):
        '''
        updates self.cpdIdDict. Only needed after modification of model
        structure, e.g. by set_cpds, add_cpd and add_cpds
        '''
        cpdIdDict = self.idx(self.cpdID)

        self.cpdIdDict = cpdIdDict
        

    def add_cpd(self, cpdID):
        '''
        adds a single compound with name cpdName (string) to cpdNames
        if it does not yet exist
        '''
        if cpdID not in self.cpdIds():
            self.cpdID.append(cpdID)
            self.updateCpdIds()



    def cpdIds(self):
        '''
        returns a dict with keys:cpdNames, values:idx
        This is now cached in self.cpdIdDict. This is updated whenever
        a compound is added.
        '''
        return self.cpdID




    def set_stoichiometry(self, reactionID, stDict):

            
        '''
        sets stoichiometry for rate rateName to values contained in stDict

        Example
        -------
        m.set_stoichiometry('v1',{'X':-1,'Y',1})

        '''

        self.stoichiometries[reactionID] = stDict

    def set_reversibility(self, reactionID, string):


        self.reversible[reactionID] = string




    def modelbase_to_cobra(self,model_ID,feedback=False):
        cobra_model=cobra.Model(model_ID)
        
        cobra_metabolites=dict()
        for ID in self.cpdID:
            formula_string=''
            for i in list(self.cpdFormula[ID]):
                formula_string+=i
                formula_string+=str(self.cpdFormula[ID][i])
            cobra_metabolites[ID]=cobra.Metabolite(ID,formula_string,ID,self.cpdCharge[ID],self.cpdCompartment[ID])
            
        cobra_reactions=dict()
        for ID in list(self.stoichiometries):

            cobra_reactions[ID]=cobra.Reaction(ID,self.reactionCommonName[ID],self.subsystems[ID],self.lower_bounds[ID],self.upper_bounds[ID])
            
            
            met_names=[]
            stoichs=[]
            for X in self.stoichiometries[ID]:
                if X in cobra_metabolites:
                    met_names.append(cobra_metabolites[X])
                    stoichs.append(self.stoichiometries[ID][X])
            stoich_dict=dict()
            for i in range(len(met_names)):    
                stoich_dict[met_names[i]]=stoichs[i]
                
                
            if feedback:    
                print (stoich_dict)
            cobra_reactions[ID].add_metabolites(stoich_dict)            

        
            cobra_model.add_reaction(cobra_reactions[ID])

        return cobra_model  




        
    def SBMLtoModelbase(self,filename):
        
        cobramodel=cobra.io.read_sbml_model(filename)

        SBML=libsbml.readSBMLFromFile(filename)
        reactions=list(SBML.model.getListOfReactions())
        
        compound_list=[]
        
        cpd_names={}
        cpd_compartments={}
        
        model_stoichometries={}
        model_reversibilities={}
        rxn_names={}
        lowerbounds={}
        upperbounds={}

        for rxn in reactions:
            rxn_id=rxn.getId()
            rxn_name=rxn.getName()
            products=list(rxn.getListOfProducts())
            educts=list(rxn.getListOfReactants())
            
            
            stoich_dict={}
            
            for prod in products:
                prod_name=prod.getSpecies()
                prod_stoi=prod.getStoichiometry()
                stoich_dict.update({prod_name:prod_stoi})
                
                if prod_name not in compound_list:
                    species=SBML.model.getSpecies(prod_name)
                    species_id=species.getId()
                    species_name=species.getName()
                    species_compartment=species.getCompartment()
                    
                    cpd_names.update({species_id:species_name})
                    cpd_compartments.update({species_id:species_compartment})
                    
                    compound_list.append(prod_name)

            for edu in educts:
                edu_name=edu.getSpecies()
                edu_stoi=edu.getStoichiometry()
                stoich_dict.update({edu_name:-1*(edu_stoi)})

                if edu_name not in compound_list:
                    species=SBML.model.getSpecies(edu_name)
                    species_id=species.getId()
                    species_name=species.getName()
                    species_compartment=species.getCompartment()
                    
                    cpd_names.update({species_id:species_name})
                    cpd_compartments.update({species_id:species_compartment})
                    
                    compound_list.append(edu_name)


                
            model_stoichometries.update({rxn_id:stoich_dict})
            rxn_names.update({rxn_id:rxn_name})
            
            bounds=cobramodel.reactions.__getattr__(rxn_id[2:]).bounds
            lowerbounds.update({rxn_id:bounds[0]})
            upperbounds.update({rxn_id:bounds[1]})
            if rxn.getReversible()==True:
                model_reversibilities.update({rxn_id:'rev'})

            else:
                model_reversibilities.update({rxn_id:'irrev'})

                
        
        for ID in list(cpd_names):

            self.set_cpdAttributes(ID,cpd_names[ID],{'Unknown':1},None,cpd_compartments[ID],'Unknown','Unknown',0.,[])
        
        for ID in list(rxn_names):

            self.set_reaction(ID,rxn_names[ID],LB=lowerbounds[ID],UB=upperbounds[ID],stoichometry_dict=model_stoichometries[ID],reversibility=model_reversibilities[ID])
                
